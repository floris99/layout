package com.example.florisresink.layout;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    Button button;
    Button button2;
    TextView text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //CHANGE TITLE ACTIVITY
        getSupportActionBar().setTitle("Activity Test");

        //CHANGE ICON ACTIVITY

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.drawable.afca);

        button = (Button) findViewById(R.id.button);

        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                if (button.getText() == "Hello") {
                    button.setText("World");
                } else {
                    button.setText("Hello");
                }

            }
        });

        text = (TextView) findViewById(R.id.text);
        button2 = (Button) findViewById(R.id.button2);

        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (text.getText() == "XXX") {
                    text.setText("YYY");
                } else {
                    text.setText("XXX");
                }
            }
        });
    }
}
