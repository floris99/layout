package com.example.florisresink.layout;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class Main2Activity extends AppCompatActivity {

    Button button3;
    TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);


        textView = (TextView) findViewById(R.id.textView);
        button3 = (Button) findViewById(R.id.button3);

        //button

        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                textView.setBackgroundColor(Color.RED);
            }
        });
    }
}
